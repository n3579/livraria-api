import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { randomInt } from 'crypto';
import { Livro } from './livro.model';
import { LivrosService } from './livros.service';

@Controller('livros')
export class LivrosController {
  constructor(
    private service: LivrosService
  ) { }

  @Get()
  async obterTodos(): Promise<Livro[]> {
    return this.service.obterTodos();
  }

  @Get(':id')
  async obterUm(@Param() params): Promise<Livro> {
    return this.service.obterUm(params.id);
  }

  @Post()
  async criar(@Body() livro: Livro){
    livro.id = randomInt(999);
    this.service.criar(livro);
  }

  @Put()
  async alterar(@Body() livro: Livro): Promise<[number]> {
    return this.service.alterar(livro);
  }

  @Delete(':id')
  async apagar(@Param() params) {
    this.service.apagar(params.id)
  }
}
